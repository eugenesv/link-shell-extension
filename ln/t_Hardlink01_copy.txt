
D:\data\cpp\hardlinks\ln>set OPTION=--copy 

D:\data\cpp\hardlinks\ln>REM  

D:\data\cpp\hardlinks\ln>REM Hardlinking Tests 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Do a simple hardlink copy 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.248       812     2.436         0         0         0
    File:         8         2         6         0         0         0
   Times:        00:00:00.002
  Folder:         1         1         -         0         0         0
 Symlink:         0         -         0         0         0         0
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
+d D:\data\cpp\hardlinks\ln\test\hardlink\dst
+f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
+f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
Junction:         0         -         0         0         0         0
ErrorLevel == 0

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Copy again, to see that nothing is copied, because the date didn't change 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst    1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.248         0         0     3.248         0         0
    File:         8         0         0         8         0         0
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
Junction:         0         -         0         0         0         0
ErrorLevel == 0

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Make the leader newer, so that it has to be copied, and all hardlinks to newly tied 

D:\data\cpp\hardlinks\ln>REM  

D:\data\cpp\hardlinks\ln>..\Tools\timestamp.exe --writetime test\hardlink\src\hl1_0.txt 

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst   1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.248       406     1.218     1.624         0         0
    File:         8         1         3         4         0         0
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
+f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
Junction:         0         -         0         0         0         0
ErrorLevel == 0

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Add an additional hardlink, only one hardlink must be additionally tied. 

D:\data\cpp\hardlinks\ln>REM  

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test test\hardlink\src\hl1_0.txt test\hardlink\src\hl1_4.txt  1>nul 

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.654         0       406     3.248         0         0
    File:         9         0         1         8         0         0
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_4.txt
.f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
Junction:         0         -         0         0         0         0
ErrorLevel == 0

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Lock the leader in the destination, so that a new leader has to be found 

D:\data\cpp\hardlinks\ln>REM But since permissions are valid for siblings of a hardlink no new leader 

D:\data\cpp\hardlinks\ln>REM can be found 

D:\data\cpp\hardlinks\ln>REM  

D:\data\cpp\hardlinks\ln>..\Tools\timestamp.exe -w test\hardlink\src\hl1_0.txt 

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.654         0         0     3.248         0       406
    File:         9         0         0         8         0         1
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
!+f(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_4.txt
Junction:         0         -         0         0         0         0
WARNING: SmartCopy finished successfully but see output for errors.
ErrorLevel == -12

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Delete 2 of 5 files from the destination, and lock the leader in the  

D:\data\cpp\hardlinks\ln>REM destination, so that a new leader has to be found 

D:\data\cpp\hardlinks\ln>REM  

D:\data\cpp\hardlinks\ln>..\Tools\timestamp.exe -w test\hardlink\src\hl1_0.txt 

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.654         0         0     2.436         0     1.218
    File:         9         0         0         6         0         3
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
!*h(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
!*h(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_4.txt
!+f(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
Junction:         0         -         0         0         0         0
WARNING: SmartCopy finished successfully but see output for errors.
ErrorLevel == -12

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM The hardlink groups have been split now. But since we newly hardlinked  

D:\data\cpp\hardlinks\ln>REM hl1_4.txt, the date in the source is newer and relinking must take place. 

D:\data\cpp\hardlinks\ln>REM  

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.654       406     1.624     1.624         0         0
    File:         9         1         4         4         0         0
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_4.txt
+f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
Junction:         0         -         0         0         0         0
ErrorLevel == 0

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Delete some hardlinks from the destination, and check that they are  

D:\data\cpp\hardlinks\ln>REM created again 

D:\data\cpp\hardlinks\ln>REM  

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.654         0       812     2.842         0         0
    File:         9         0         2         7         0         0
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_4.txt
.f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
Junction:         0         -         0         0         0         0
ErrorLevel == 0

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Lock the hardlink tupel in the destination. But since 

D:\data\cpp\hardlinks\ln>REM nothing has changed, no error is displayed 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.654         0         0     1.624         0     2.030
    File:         9         0         0         4         0         5
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
!*h(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
!*h(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
!*h(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
!*h(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_4.txt
!+f(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
Junction:         0         -         0         0         0         0
WARNING: SmartCopy finished successfully but see output for errors.
ErrorLevel == -12

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Do it again, but have a file with read only in the destination 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>attrib +r test\hardlink\dst\hl0_0.txt 

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.654       406     2.842       406         0         0
    File:         9         1         7         1         0         0
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
*h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_4.txt
.f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
+f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
Junction:         0         -         0         0         0         0
ErrorLevel == 0

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Do it again, but have a file with attributes so that it cant be accessed 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\hardlink\src test\hardlink\dst  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.654         0         0     2.030         0     1.624
    File:         9         0         0         5         0         4
   Times:        00:00:00.002
  Folder:         1         0         -         0         0         0
 Symlink:         0         -         0         0         0         0
!*h(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_1.txt
!*h(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_2.txt
!*h(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_3.txt
!+f(0x00000005) D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl0_0.txt
=f D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_0.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_1.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_2.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_3.txt
=h D:\data\cpp\hardlinks\ln\test\hardlink\dst\hl1_4.txt
Junction:         0         -         0         0         0         0
WARNING: SmartCopy finished successfully but see output for errors.
ErrorLevel == -12

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM Test the subst driveletters hardlinking 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --symbolic test\hardlink\src\ln_hardlink.h 
test\hardlink\src\ln_hardlink.h:3

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --symbolic test\hardlink\src\ln_hardlink2.h 
test\hardlink\src\ln_hardlink2.h:3

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --list t:\ln_hardlink2.h 
D:\data\cpp\hardlinks\ln\test\hardlink\src\ln.h
D:\data\cpp\hardlinks\ln\test\hardlink\src\ln_hardlink.h
D:\data\cpp\hardlinks\ln\test\hardlink\src\ln_hardlink2.h

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --list test\hardlink\src\ln_hardlink2.h 
D:\data\cpp\hardlinks\ln\test\hardlink\src\ln.h
D:\data\cpp\hardlinks\ln\test\hardlink\src\ln_hardlink.h
D:\data\cpp\hardlinks\ln\test\hardlink\src\ln_hardlink2.h

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --symbolic test\hardlink\src\ln_hardlink3.h 
test\hardlink\src\ln_hardlink3.h:4
